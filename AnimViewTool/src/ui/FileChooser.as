package ui
{
	import com.bit101.components.Component;
	import com.bit101.components.InputText;
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	
	public class FileChooser extends Component
	{
		public var filter : Array = [];
		public var onComplete : Function;
		
		private var _label : Label = new Label();
		private var _file : FileReference;
		private var _filePath : InputText = new InputText();
		private var _button : PushButton = new PushButton();
		
		override protected function addChildren() : void
		{
			super.addChildren();
			
			_button.x = 125;
			_button.width = 75;
			_button.label = "Browse";
			_button.addEventListener(MouseEvent.CLICK, onButtonClicked);
			
			_filePath.enabled = false;
			_filePath.width = 120;
			_filePath.height = _button.height;
			
			_button.y = _filePath.y = 20;
			
			addChild(_filePath);
			addChild(_button);
			addChild(_label);
		}
		
		private function onButtonClicked(event : MouseEvent) : void
		{
			if (_file) _file.browse(filter);
		}
		
		private function onFileSelected(event : Event) : void
		{
			_filePath.text = _file.name;
			_file.addEventListener(Event.COMPLETE, onFileComplete);
			_file.load();
		}
		
		private function onFileComplete(event : Event) : void
		{
			if (onComplete != null) onComplete();
		}
		
		override public function set width(w : Number) : void
		{
			super.width = w;
			_button.x = w - _button.width;
			_filePath.width = w - _button.width - 5;
		}
		
		public function get label() : String
		{
			return _label.text;
		}
		
		public function set label( value : String ) : void
		{
			_label.text = value;
		}
		
		public function get file() : FileReference
		{
			return _file;
		}
		
		public function set file( value : FileReference ) : void
		{
			if (_file)
			{
				_file.removeEventListener(Event.SELECT, onFileSelected);
			}
			
			_file = value;
			_file.addEventListener(Event.SELECT, onFileSelected);
			
			if(_file.data)
			{
				_filePath.text = _file.name;
			}
		}
	}
}