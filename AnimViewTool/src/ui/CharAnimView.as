package ui
{
	import com.adobe.images.PNGEncoder;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	
	import fw.anim.AnimSprite;
	
	import model.AppModel;
	
	public class CharAnimView extends Sprite
	{
		private var _appModel:AppModel;
		private var _charAnim:AnimSprite;
		private var _panel:OptionsPanel;
		
		private var curTime:int=0;
		private var lastTime:int=0;
		
		private var rcBounds:Rectangle;
		private var offMat:Matrix;
		
		public function CharAnimView(model:AppModel)
		{
			super();
			
			_appModel= model;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			
		}
		
		protected function onAdded(event:Event):void
		{
			createPanel(parent);
			
			curTime= getTimer();
			lastTime= curTime;
		}
		
		public function initAnim(anim:AnimSprite):void
		{
			_charAnim= anim;
			_charAnim.smoothing= true;
			
			addChild(_charAnim);
			
			//_charAnim.play("idle");
			//_charAnim.updateAnimation();
			
			rcBounds= _charAnim.getBounds(this);
			//rcBounds.inflate(-40, -20);
			//rcBounds.offset(-35, 12);
			
			centerView();
			drawBounds(true);
			
			_panel.setInfo(_charAnim.numFrames, _charAnim);
			
			addEventListener(Event.ENTER_FRAME, onFrameUpdate);
			
			_expBitmap= new BitmapData(rcBounds.width, rcBounds.height, true, 0);
			_expImage= new Bitmap(_expBitmap);
			this.parent.addChild(_expImage);
			
			_expImage.x= 20;
			_expImage.y= 660 - rcBounds.height;
		}
		
		public function centerView():void
		{
			if (_charAnim) {
				x = (510 - _charAnim.bitmapData.rect.width * scaleX) /2;
				y = (660 - _charAnim.bitmapData.rect.height * scaleY) /2;
			}
		
		}
		
		public function setFrameRate(fps:int):void
		{
			if (_charAnim && _charAnim.fps != fps) {
				_charAnim.forceFrameRate(fps);
				_appModel.log("Frame rate set to " + fps + " FPS");
			}
		}
		
		private function createPanel(rootApp:DisplayObjectContainer):void
		{
			_panel = new OptionsPanel(_appModel);
			_panel.x= 510; _panel.y=24;
			rootApp.addChild(_panel);
			
			_panel.createGUI(this);
			
		}		
		
		public function drawBounds(show:Boolean):void
		{
			graphics.clear();
			if (!show) {
				return;
			}
			
			var rc:Rectangle = _charAnim.bitmapData.rect;
			graphics.lineStyle(0.5,0);
			graphics.drawRect(rc.x, rc.y, rc.width, rc.height);
		}
		
		
		protected function onFrameUpdate(event:Event):void
		{
			curTime = getTimer()
			
			_charAnim.updateAnimation(curTime-lastTime);
			lastTime= curTime;
			
			_panel.updateFrame();
		}
		
		//////////
		// Export
		
		private var _expImage:Bitmap;
		private var _expBitmap:BitmapData;
		
		private var fs:FileStream= new FileStream();
		
		public function exportSeq(seq:String, basePath:String):void
		{
			//_charAnim.reset(seq);
			_charAnim.play(seq);
			_charAnim.stop(true);
			
			var numFrames:int= _panel.numFramesInSeq;
			
			for (var i:int = 0; i < numFrames; i++) 	{
				TweenMax.delayedCall(i*0.3, exportFrame, [i, basePath]);
			}
			
		}
		
		private function exportFrame(frameId:int, basePath:String):void
		{
			_appModel.log("Exporting frame " + frameId);
			
			_expBitmap.fillRect(_expBitmap.rect, 0);
			_expBitmap.draw(_charAnim, offMat);
			
			var idStr:String= frameId.toString();
			if (frameId<10) idStr= "0" + frameId;
			
			var filePath:String= basePath + idStr +".png";
			
			
			var imgBytes:ByteArray = PNGEncoder.encode(_expBitmap);
			
			var file:File= new File(filePath);
			fs.open(file, FileMode.WRITE);
			fs.writeBytes(imgBytes);
			fs.close();
			
			_panel.advanceFrame();
		}
			
	}
}